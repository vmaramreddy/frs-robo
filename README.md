#Frs Robo #

Frs Robo supports browser automation in .Net to help make tests readable, robust, fast to write and less tightly coupled to the UI. If tests are littered with sleeps, retries, complex X Path expressions and IDs dug out of the source with FireBug then Frs Robo might help.

### What is this for? ###

This library is going to reduce  huge  amount of unnecessary code so that test automation engineers can support future product releases quickly . This library gives a happy rest to
both automation engineers and functional teams by reducing huge amount of rework of the
written test cases.

### How do I get set up? ###
* Download Visual Studio
* Configure nuget package manager to VS 
* Download product test cases from Perforce server
* Install Nunit
* Run 

### Contribution guidelines ###

* Vijay Bhaskar Reddy Maramreddy
  [vijay.maramreddy@frontrange.com]


### Who do I talk to? ###

* Swathi Bongu
* Ulrica Hung
* Kelly Tang
* Angelina

### Continuous Integration ###
Jennkins CI