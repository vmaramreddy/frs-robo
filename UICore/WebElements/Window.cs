﻿using OpenQA.Selenium;

namespace UICore.WebElements
{
    class Window : BasicForm
    {
        public Window(IWebDriver driver, ExtJSQueryType queryType, string query) :
               base(driver, queryType, query)
        {
            
        }

        public Window(IWebDriver driver, IWebElement topElement) :
               base (driver, topElement)
        {
            
        }

        /**
         * Method close.
         */
        public void close()
        {
            topElement.FindElement(By.XPath(".//div[contains(@class, 'x-tool-close')]")).Click();
        }
    }
}
