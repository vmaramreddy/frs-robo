﻿using System;
using OpenQA.Selenium;

namespace UICore.WebElements
{
    class TreeNode : ExtJSComponent
    {
        string getUIfunction = ".getUI()";
	
	    /**
	     * Field nodeExpression.
	     */
	    private String nodeExpression = "";
        /**
         * Field nodeUiExpression.
         */
        private String nodeUiExpression = "";
        /**
         * Field treeExpression.
         */
        private String treeExpression = "";

        public TreeNode(IWebDriver driver, ExtJSQueryType queryType, string query) :
               base(driver, queryType, query)
        {
            treeExpression = getExpression();
        }

        /**
         * collapse the Selected node
         */
        public void collapseNode()
        {
            nodeExpression = ".getSelectionModel().getSelectedNode()";
            execScriptClean(string.Format("%s.collapse()", nodeExpression));
        }

        /**
         * collapse the root Node
         */
        public void collapseRootNode()
        {
            nodeExpression = ".getSelectionModel().getSelectedNode()";
            execScriptClean(string.Format("%s.collapse()", getRootNode()));
        }

        /**
         * Expand this node. the node must be selected;
         */
        public void expand()
        {
            getSelectedNode();
            execScriptClean(string.Format("%s%s.expand()", treeExpression, nodeExpression));
        }

        /**
         * expend the Selected node
         */
        public void expentNode()
        {
            nodeExpression = ".getSelectionModel().getSelectedNode()";
            execScriptClean(string.Format("%s.expand()", nodeExpression));
        }

        /**
         * expend the root Node
         */
        public void expentRootNode()
        {
            nodeExpression = ".getSelectionModel().getSelectedNode()";
            execScriptClean(string.Format("%s.expand()", getRootNode()));
        }

        /**
         * Trying to search in tree and find a child with given value if found the node will be select
         * 
         * @param val
         * @return - selected node (if found one)
         */
        public TreeNode findChild(string val)
        {
            if (string.IsNullOrEmpty(nodeExpression)
                    || nodeExpression.Length == 0)
            {
                getRootNode();
            }
            execScriptOnExtJsCmp(nodeExpression
                                + ".findChild( 'name' ,'"
                                + val
                                + "' , true ).select()");
            return this;
        }

        /**
         * find specific node in tree by given attribute and SELECT him
         * 
         * @param attribute
         * @param value
         * @return
         */
        public TreeNode findChild(string attribute, string value)
        {
            execScriptClean(string.Format("%s.getRootNode().findChild('%s','%s',true).select()", treeExpression, attribute, value));
            return this;
        }

        /**
         * Method findNodeGridChild.
         * 
         * @param val
         *            String
         * @return TreeNode
         */
        public TreeNode findNodeGridChild(string val)
        {
            if (string.IsNullOrEmpty(nodeExpression)
                    || nodeExpression.Length == 0)
            {
                getRootNode();
            }
            execScriptClean(treeExpression
                            + nodeExpression
                            + ".findChild( 'name' ,"
                            + "\"<span style='color:black'>"
                            + val
                            + "</span>\" , true ).select()");
            return this;
        }

        /**
         * Method getCell.
         * 
         * @param index
         *            Integer
         * @return String
         */
        public string getCell(int index)
        {
            if (string.IsNullOrEmpty(nodeUiExpression))
            {
                getNodeUI();
            }

            return ((string)(execScriptClean(treeExpression
                            + nodeUiExpression
                            + ".childNodes["
                            + index
                            + "].firstChild.innerHTML"))).Replace("\\<.*?>", "");
        }

        /**
         * return child count of selected node
         * 
         * @return - the count of node child
         */
        public int getLength()
        {
            return int.Parse((string)execScriptClean(string.Format("%s%s.childNodes.length", treeExpression, nodeExpression)));
        }

        /**
         * return Node attribute
         * 
         * @param att
         * @return String
         */
        public string getNodeAtt(string att)
        {
            return execScriptOnExtJsCmpReturnString(nodeExpression
                                                    + ".attributes."
                                                    + att);
        }

        /**
         * return node UI of the selected node
         * 
         * @return String
         */
        public string getNodeUI()
        {
            nodeUiExpression = nodeExpression
                             + ".ui.getEl().firstChild";
            return (string)execScriptClean(treeExpression
                                            + nodeExpression
                                            + ".ui.getEl().firstChild");
        }

        /**
         * return the root node of given Tree
         * 
         * @return TreeNode
         */
        public TreeNode getRootNode()
        {
            nodeExpression = ".getRootNode()";
            execScriptClean(string.Format("%s.%s", treeExpression, nodeExpression));
            return this;
        }

        /**
         * return the Selected node
         * 
         * @return TreeNode
         */

        public TreeNode getSelectedNode()
        {
            nodeExpression = ".getSelectionModel().getSelectedNode()";
            execScriptClean(treeExpression + nodeExpression);
            return this;
        }

        /**
            * return the value by given attribute (the node should be selected)
            * 
            * @param att
            * @return String
            */
        public String getSelectedNodeAtt(string att)
        {
            return execScriptOnExtJsCmpReturnString("return extCmp.getSelectionModel().getSelectedNode().attributes." + att);
        }

        /**
         * check if the selected node has chides
         * 
         * @return boolean
         */
        public bool hasChildNodes()
        {
            return execScriptOnExtJsCmpReturnBoolean(string.Format("%s.hasChildNodes() ", nodeExpression));
        }

        /**
         * @return
         */
        public bool isChecked()
        {
            return execScriptOnExtJsCmpReturnBoolean(string.Format("%s.isChecked() ", treeExpression
                    + getUIfunction));
        }

        /**
         * Returns true if the selected node is the first child of its parent
         * 
         * @return boolean
         */
        public bool isFirst()
        {
            return execScriptOnExtJsCmpReturnBoolean(string.Format("%s.isFirst() ", nodeExpression));
        }

        /**
         * Returns true if the selected node node is a leaf
         * 
         * @return boolean
         */
        public bool isLeaf()
        {
            return execScriptOnExtJsCmpReturnBoolean(string.Format("%s.isLeaf() ", nodeExpression));
        }

        /**
         * Returns true if the selected node is Selected
         * 
         * @return boolean
         */
        public bool isSelected()
        {
            return execScriptOnExtJsCmpReturnBoolean(String.Format("%s.isSelected() ", nodeExpression));
        }

        /**
         * Method select.
         * 
         * @param id
         *            String
         * @return TreeNode
         */
        public TreeNode select(string id)
        {
            execScriptClean(".getSelectionModel().select(" //
                            + getExpression()
                            + ".nodeHash['"
                            + id
                            + "']"
                            + //
                            ")");
            return getSelectedNode();
        }

        /**
         * Sets the checked status of the tree node to the passed value . The node must be selected
         * 
         * @param check
         */
        public void toggleCheck(bool check)
        {
            execScriptClean(string.Format("%s.getSelectionModel().getSelectedNode().getUI().toggleCheck(%s)", treeExpression, check));
        }

        /**
         * try to select an node and Sets the checked status of the tree node to the passed value .
         * 
         * @param attribute
         * @param value
         * @param check
         */
        public void toggleCheck(string attribute, string value, bool check)
        {
            findChild(attribute, value);
            getSelectedNode();
            execScriptClean(String.Format("%s%s%s.toggleCheck(%s)", treeExpression, nodeExpression, getUIfunction, check));
        }

    }
}
