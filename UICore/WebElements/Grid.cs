﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace UICore.WebElements
{
    class Grid : ExtJSComponent
    {
        /**
	     * Field gridExp.
	     */
        string gridExp = "";

        public Grid(IWebDriver driver, ExtJSQueryType queryType, string query) :
                    base(driver, queryType, query)
        {
             gridExp = getExpression();
        }

        public Grid(IWebDriver driver, IWebElement topElement) :
                    base (driver, topElement)
        {
              gridExp = getExpression();
        }

        /**
         * Click on selected cell (with link inside) by given row and column
         * 
         * @deprecated This function just doesnt look like it works. its too general.
         * @param row
         * @param col
         * @return Grid
         */
        
        public Grid clickOnCell(int row,int col)
        {
            IWebElement cell = topElement.FindElement(By.XPath(string.Format(".//*[contains(text(),'%s')]", getCellValue(row, col))));
            cell.Click();
            return this;
        }

        /**
         * clicks on a dom object (with the specified CSS class) residing inside a grid cell
         * 
         * @param row
         *            - the row of the cell
         * @param col
         *            - the column of the cell
         * @param cssClass
         *            - the CSS class of the DOM object inside the grid cell, upon which the click should be fired
         * @return Grid - returns the object (this)
         */

        public Grid clickOnItemWithClassInCell(int row, int col, string cssClass)
        {
            string cellDomExpression = getCellDomObjectExpression(row, col);
            string xpath = Utils.ExtJsUtils.getComponentXpath(driver, cellDomExpression);
            xpath += "//*[contains(@class, \""
                    + cssClass
                    + "\")]";
            driver.FindElement(By.XPath(xpath)).Click();

            return this;
        }

        /**
         * Method contains.
         * 
         * @param key
         *            String
         * @return boolean
         */
        public bool contains(string key)
        {
            string[] keys = getKeys();
            foreach (string key2 in keys)
            {
                if (key.Equals(key2))
                {
                    return true;
                }

            }

            return false;

        }

        /**
         * Double click on an editable cell by given row and column
         * 
         * @param row
         * @param col
         * @return Grid
         */
        public Grid doubleClickOnEditableCell(int row, int col)
        {
            string cellDomExpression = getCellDomObjectExpression(row, col);
            string xpath = Utils.ExtJsUtils.getComponentXpath(driver, cellDomExpression);
            IWebElement cell = driver.FindElement(By.XPath(xpath));
            Actions actions = new Actions(driver);
            actions.DoubleClick(cell);
            actions.Perform();
            return this;
        }

        /**
         * If the cell in an editor grid is currently being edited, change its value to the speicified one
         * 
         * @param row
         * @param col
         * @param value
         * @return Grid
         */

        public Grid editCellAndSetValue(int row, int col, string value)
        {
            execScriptClean(string.Format("%s.getColumnModel().getCellEditor(%d,%d).setValue('%s')", gridExp, col, row, value));
            return this;
        }

        /**
         * searches for a row with a specific value in a specified column, and returns the row index
         * 
         * @param col
         *            - the column to search over
         * @param requiredValue
         *            - the required value to look for
         * @return int - the index of the column (starting from 0)
         */

        public int findRowIndexByColumnValue(int col, string requiredValue)
        {
            string columnDataIndex = (string)execScriptOnExtJsCmp(string.Format("return extCmp.getColumnModel().getDataIndex(%d)", col));
            return int.Parse((string)execScriptOnExtJsCmp(string.Format(".getStore().find('%s', '%s')", columnDataIndex, requiredValue)));
        }

        /**
         * return the dom object EXPRESSION of inner cell This would only be translated to the DOM object in the client-side upon selenium.eval
         * 
         * @param row
         * @param col
         * @return String
         */
        public string getCellDomObjectExpression(int row, int col)
        {
            return string.Format("window.Ext.fly(%s.view.getCell(%d,%d)).dom", gridExp, row, col);
        }

        /**
         * Get the inner html of given cell.
         * 
         * @param row
         *            starts from 0
         * @param col
         *            starts from 0
         * @return String
         */
        public string getCellValue(int row, int col)
        {
            string colExp = string.Format("window.Ext.fly(%s.view.getCell(%d,%d)).dom.textContent", gridExp, row, col);
            return (string)execScriptClean(colExp);
        }

        /**
         * return the header of the grid by given column index
         * 
         * @param colIndex
         * @return String
         */
        public string getColumnHeader(int colIndex)
        {
            return (string)execScriptOnExtJsCmp(string.Format("return extCmp.getColumnModel().getColumnHeader(%d)", colIndex));
        }

        /**
         * return the ui row count (just what the user see)
         * 
         * @return Long
         */
        public long getGridRowCount()
        {
            waitToLoad();
            return (long) execScriptOnExtJsCmp("return extCmp.view.getRows().length");
        }

        /**
         * Method getGridStoreCount.
         * 
         * @return Integer
         */
        public long getGridStoreCount()
        {
            waitToLoad();
            return (long)execScriptOnExtJsCmp("extCmp.getStore().data.length");
        }

        /**
         * @return array of id
         */
        public string[] getKeys()
        {
            return ((string)execScriptOnExtJsCmp("return el.getStore().data.keys")).Split(',');
        }

        /**
         * return selected row index
         * 
         * @return int
         */
        public int getSelectedIndex()
        {
            int length = getStoreDataLength();
            for (int i = 0; i < length; i++)
            {
                if (isSelected(i))
                {
                    return i;
                }
            }

            return -1;
        }

        /**
         * return count of grid data
         * 
         * @return (int) count of grid data
         */
        public int getStoreDataLength()
        {
            int length = (int) execScriptOnExtJsCmp("return extCmp.getStore().data.length");
            return length;
        }

        /**
         * return True if given Index row is selected
         * 
         * @param index
         * @return boolean
         */
        public bool isSelected(int index)
        {
            return execScriptOnExtJsCmpReturnBoolean("return extCmp.getSelectionModel().isSelected("
                    + index
                    + ")");
        }

        /**
         * Method openContextMenu.
         * 
         * @param rowNumber
         *            int
         * @param colNumber
         *            int
         * @param menuId
         *            String
         * @return Menu
         */
        public Menu openContextMenu(int rowNumber, int colNumber, string menuId)
        {
            string[] keys = getKeys();
            return openContextMenu(keys[rowNumber], colNumber, menuId);
        }

        /**
         * Method openContextMenu.
         * 
         * @param selectedKey
         *            String
         * @param colNumber
         *            int
         * @param menuId
         *            String
         * @return Menu
         */
        public Menu openContextMenu(string selectedKey, int colNumber, string menuId)
        {
            IWebElement el = driver.FindElement(By.XPath(getComponentId()
                    + "_"
                    + selectedKey
                    + "_col"
                    + colNumber));

            Actions actions = new Actions(driver);
            actions.ContextClick(el);
            actions.Perform();

            return new Menu(driver, ExtJSQueryType.GetCmp, menuId);
        }

        /**
         * Method openContextMenu.
         * 
         * @param selectedKey
         *            String
         * @param menuId
         *            String
         * @return Menu
         */
        public Menu openContextMenu(string selectedKey, string menuId)
        {
            return openContextMenu(selectedKey, 1, menuId);
        }

        /**
         * Select ui-row by given row index
         * 
         * @param index
         *            - (int) row Index start from 0
         * @return Grid
         */
        public Grid select(int index)
        {
            waitToLoad();
            execScriptOnExtJsCmp("extCmp.getSelectionModel().selectRow("
                    + index
                    + ")");

            return this;
        }

        /**
         * Method select.
         * 
         * @param key
         *            String
         * @return Grid
         */
        public Grid select(string key)
        {
            // Didn't worked!
            // String eval = getEval( ".getStore().data.indexOfKey('" + key + "')" );
            string[] keys = getKeys();
            for (int i = 0; i < keys.Length; i++)
            {
                if (key.Equals(keys[i]))
                {
                    select(i);

                    return this;
                }
            }
            throw new Exception("Unable to select" + key);
        }

        /**
         * Select row by given Text to search and cell - (where to search)
         * 
         * @param text
         *            - any text
         * @param cellIndex
         *            - search the text in the cell start from 0
         * @return selected (int) index
         */
        public int select(string text, int cellIndex)
        {
            waitToLoad();
            int gridCount = getStoreDataLength();
            for (int i = 0; i < gridCount; i++)
            {
                if (getCellValue(i, cellIndex).Equals(text))
                {
                    select(i);
                    return i;
                }
            }
            return -1;
        }

        /**
         * wait until the mask disappear
         */
        public void waitForLoading()
        {
            waitForGridLoadingMaskToDisappear(getComponentId());
        }

    }
}
