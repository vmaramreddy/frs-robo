﻿using OpenQA.Selenium;

namespace UICore.WebElements
{
    class Radio : ExtJSComponent
    {
        public Radio(IWebDriver driver, ExtJSQueryType queryType, string query) :
               base (driver, queryType, query)
        {
            
        }

        public Radio(IWebDriver driver, IWebElement topElement) :
               base (driver, topElement)
        {
            
        }

        /**
         * @return boolean
         */
        public bool isChecked()
        {
            return execScriptOnExtJsCmpReturnBoolean("return extCmp.checked");
        }

    }
}
