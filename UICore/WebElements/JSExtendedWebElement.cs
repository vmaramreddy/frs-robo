﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace UICore.WebElements
{
    public class JSExtendedWebElement 
    {
        protected internal IJavaScriptExecutor js = null;

        protected internal static readonly string FUNCTION_DEFINE_SExt = "if( typeof SExt === \"undefined\" ){ SExt = function(){ };" 
            + "SExt.log = function( arg ) { if( console && console.log ) console.log( arg ) } };";

        #pragma warning disable

        private static readonly string FUNCTION_highlight = "SExt.highlight = function(element, timesec) {"
            + "  var prevBackgroundColor = element.style.backgroundColor;"
            + "  var prevBorder = element.style.border;"
            + "  element.style.backgroundColor = \"#FDFF47\";"
            + "  element.style.border = \"3px solid #11FF11\";"
            + "  window.setTimeout( function (element, prevBackgroundColor, prevBorder) {"
            + "    element.style.backgroundColor = prevBackgroundColor;"
            + "    element.style.border = prevBorder;"
            + "  }, timesec * 1000, element, prevBackgroundColor, prevBorder);"
            + "}";

        private static readonly string FUNCTION_htmlEscape = "SExt.htmlEscape = function(str) {"
            + "return String(str) "
            + "	.replace(/&/g, '&amp;') "
            + "	.replace(/\"/g, '&quot;')"
            + "	.replace(/'/g, '&#39;')"
            + "	.replace(/</g, '&lt;')"
            + "	.replace(/>/g, '&gt;');"
            + "}";

        #pragma warning restore

        protected internal static long sleepInMillis = 300L;

        protected internal static long timeOutInSeconds = 5L;

        protected internal IWebDriver driver;

        protected internal IWebElement topElement;

        protected internal WebDriverWait wait;

        public JSExtendedWebElement(IWebDriver driver, string jsCode)
        {
            setDriverAndFindElementByScript(driver, jsCode);
        }

        public JSExtendedWebElement(IWebDriver driver, IWebElement topElement)
        {
            setDriver(driver);
            setElement(topElement);
        }

        public virtual void click()
        {
            topElement.Click();
        }

        private void setDriverAndFindElementByScript(IWebDriver driver, string jsCode)
        {
            setDriver(driver);
            IWebElement iwebElement;
            try
            {
                iwebElement = (IWebElement)js.ExecuteScript(jsCode, new object[0]);
            }
            catch (InvalidCastException)
            {
                iwebElement = null;
            }
            topElement = iwebElement;
        }

        protected internal void setDriver(IWebDriver driver)
        {
            this.driver = driver;
            js = driver as IJavaScriptExecutor;
            wait = new WebDriverWait(new SystemClock(), driver, TimeSpan.FromSeconds(timeOutInSeconds), TimeSpan.FromMilliseconds(sleepInMillis));
        }

        protected internal void setElement(IWebElement el)
        {
            topElement = el;
        }

        public virtual bool disabled()
        {
            return execScriptOnTopLevelElementReturnBoolean("return el.disabled");
        }

        protected internal object execScriptClean(string expr)
        {
            waitForFinishAjaxRequest();
            return js.ExecuteScript(expr, new object[0]);
        }

        public bool execScriptCleanReturnBoolean(string expr)
        {
            try
            {
                object obj = execScriptClean(expr);
                if (bool.TrueString.Equals(obj) || bool.TrueString.Equals(bool.Parse((string)obj) ? 1 : 0))
                    return true;
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public bool execScriptCleanReturnIsNull(string expr)
        {
            try
            {
                object obj = execScriptClean(expr);
                return obj == null || "null".Equals(obj);
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected internal object execScriptOnElement(string jsCode, IWebElement element)
        {
            string str1 = string.Format("var el = arguments[0]; %s;", jsCode);
            waitForFinishAjaxRequest();
            IJavaScriptExecutor ijavaScriptExecutor = js;
            string str2 = str1;
            object[] objArray = new object[1];
            int index = 0;
            IWebElement iwebElement = element;
            objArray[index] = iwebElement;
            return ijavaScriptExecutor.ExecuteScript(str2, objArray);
        }

        protected internal object execScriptOnTopLevelElement(string jsCode)
        {
            return execScriptOnElement(jsCode, topElement);
        }

        protected internal bool execScriptOnTopLevelElementReturnBoolean(string jsCode)
        {
            try
            {
                object obj = execScriptOnElement(jsCode, topElement);
                if (bool.TrueString.Equals(obj) || bool.TrueString.Equals(bool.Parse((string)obj) ? 1 : 0))
                    return true;
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        protected internal bool waitForExecScriptToReturnTrue(string fullExpr)
        {
            for (int second = 0; second < timeOutInSeconds; ++second)
            {
                try
                {
                    if (bool.TrueString.Equals(execScriptCleanReturnBoolean(fullExpr) ? 1 : 0))
                        break;
                }
                catch (Exception)
                {

                }
                try
                {
                    Thread.Sleep((int)sleepInMillis);
                    continue;
                }
                catch (Exception)
                {
                    continue;
                }

               
            }
            return true;
        }

        /**
	    * Wait until all ajax requests are finished. This should be specified by each framework.
	    * 
	    * @return boolean return true if all ajax requests are finished.
	    */

        protected internal virtual bool waitForFinishAjaxRequest()
        {
            return true;
        }

       
    }
}
