﻿using OpenQA.Selenium;

namespace UICore.WebElements
{
    class Checkbox : ExtJSComponent
    {
        #pragma warning disable
        string setCheckbox = "SExt.prototype.setCheckbox = function (query, value, uuid) {"
            + "var comp = this.findVisibleComponent(query);"
            + "var success = comp.setValue(value);"
            + "writeDataToDiv(success, uuid);"
            + "}";
        #pragma warning restore
        public Checkbox(IWebDriver driver, ExtJSQueryType queryType, string query) :
            base(driver, queryType, query)
        {
            
        }

        public Checkbox(IWebDriver driver, IWebElement topElement) :
            base(driver, topElement)
        {
            
        }

        /**
         * check() sets checkbox to checked
         */
        public void check()
        {
            check(true);
        }

        /**
         * check(true) or check(false) to set checkbox to checked or unchecked.
         * 
         * @param enable
         *            boolean
         */
        public void check(bool enable)
        {
            if (enable != (bool)execScriptOnExtJsCmp("return extCmp.getValue()"))
            {
                click();
            }
            execScriptOnExtJsCmp("extCmp.setValue("
                    + enable
                    + ")");
        }

        /**
         * @return boolean
         */
        public bool isChecked()
        {
            bool ret = (bool)execScriptOnExtJsCmp("return extCmp.checked;");
            return ret;
        }

        /**
         * uncheck() sets checkbox to unchecked
         */
        public void uncheck()
        {
            check(false);
        }

    }
}
