﻿using OpenQA.Selenium;

namespace UICore.WebElements
{
    class TabPanel : ExtJSComponent
    {
        /**
	     * Field tabIndex.
	     */
        private int tabIndex;

        public TabPanel(IWebDriver driver, ExtJSQueryType queryType, string query, int tabIndex) :
               base(driver, queryType, query)
        {
            this.tabIndex = tabIndex;
        }

        public TabPanel(IWebDriver driver, IWebElement topElement, int tabIndex) :
               base (driver, topElement)
        {
            this.tabIndex = tabIndex;
        }

        /**
         * try to collapse the selected tab
         */
        public void collapse()
        {
            execScriptOnExtJsCmp("extCmp.collapse()");
            waitForExecScriptOnExtJSCmpToReturnTrue("return extCmp.collapsed == true");
        }

        /**
         * try to expend the selected tab
         */
        public void expand()
        {
            execScriptOnExtJsCmp("return extCmp.expand()");
            waitForExecScriptOnExtJSCmpToReturnTrue("return extCmp.collapsed == false");
        }

        /**
         * @return - (int) the current index of selected Tab
         */
        public int getCurentIndexTab()
        {
            string activeTabId = (string)execScriptClean(string.Format("%s.getActiveTab().getActiveTab().id", getExpression()));
            return int.Parse((string)execScriptClean(string.Format("%s.items.indexOf('%s')", getExpression(), activeTabId)));
        }

        /**
         * return All id's of the current parent tabPanel
         * 
         * @return String[]
         */
        public string[] getKeys()
        {
            string keys = (string) execScriptOnExtJsCmp("return extCmp.data.keys");
            return keys.Split(',');
        }

        /**
         * Method getTabIndex.
         * 
         * @return Integer
         */
        public int getTabIndex()
        {
            return tabIndex;
        }

        /**
         * Set active by given index of wanted panel
         * 
         * @param indexPanel
         */
        public void setActiveTab(int indexPanel)
        {
            execScriptClean(string.Format("%s.setActiveTab(%d)", getExpression(), indexPanel));
        }

        /**
         * set Active tab after the search
         */
        public void setAsActiveTab()
        {
            setActiveTab(tabIndex);
        }

        /**
         * Method setTabIndex.
         * 
         * @param tabIndex
         *            Integer
         */
        public void setTabIndex(int tabIndex)
        {
            this.tabIndex = tabIndex;
        }
    }
}
