﻿using OpenQA.Selenium;

namespace UICore.WebElements
{
    class Chart : ExtJSComponent
    {
        string FUNCTION_convertStoreToCSV = "SExt.convertStoreToCSV = function(store) {"
            + "  var fields = Ext.Array.map(store.model.getFields(), function(field) {"
            + "    return field.name;"
            + "  });"
            + "  var csvLines = ['\"' + fields.join('\",\"') + '\"'];"
            + "  Ext.each(store.getRange(), function(modelObj) {"
            + "    var propList = [];"
            + "    Ext.Array.forEach(fields, function(fieldName) {"
            + "      propList.push(modelObj.data[fieldName]);"
            + "    });"
            + "    csvLines.push('\"' + propList.join('\",\"') + '\"');"
            + "  });"
            + "  return csvLines.join(\"\\n\");"
            + "}";

        string FUNCTION_convertStoreToJSON = "SExt.convertStoreToJSON = function (store) {"
                + "  if(!store){"
                + "	   return \"\";"
                + "  }"
                + "  var dataList = Ext.Array.map(store.getRange(), function(entry, list) {"
                + "    var data = entry.data;"
                + "    return data;"
                + "  });"
            // Write the first only
            + "  return Ext.encode(dataList);"
                + "}";

        public Chart(IWebDriver driver, ExtJSQueryType queryType, string query) : base(driver, queryType, query)
        {
            
        }

        public Chart(IWebDriver driver, IWebElement topElement) : base(driver, topElement)
        {
            
        }

        public string getChartAsJSON()
        {
            execScriptClean(FUNCTION_convertStoreToJSON);
            string jsCode = "return SExt.convertStoreToJSON(extCmp.getStore());";
            return (string)execScriptOnExtJsCmp(jsCode);
        }

        public string getChartAsCSV()
        {
            execScriptClean(FUNCTION_convertStoreToCSV);
            string jsCode = "return SExt.convertStoreToCSV(extCmp.getStore());";
            return (string)execScriptOnExtJsCmp(jsCode);
        }

    }
}
