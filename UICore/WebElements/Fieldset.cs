﻿using OpenQA.Selenium;

namespace UICore.WebElements
{
    public class Fieldset : ExtJSComponent
    {
        /**
	     * Field checkbox.
	     */
        private Checkbox checkbox = null;

        public Fieldset(IWebDriver driver, ExtJSQueryType queryType, string query) :
                        base(driver, queryType, query)
        {
            
        }

        public Fieldset(IWebDriver driver, IWebElement topElement) :
                        base(driver, topElement)
        {
            
        }

        /**
         * Method check.
         * 
         * @param enable
         *            boolean
         * @return Fieldset
         */
        public Fieldset check(bool enable)
        {
            checkbox.check(enable);

            return this;
        }
    }
}
