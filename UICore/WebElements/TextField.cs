﻿using OpenQA.Selenium;

namespace UICore.WebElements
{
    class TextField : Field
    {
        public TextField(IWebDriver driver, ExtJSQueryType queryType, string query) :
               base(driver, queryType, query)
        {
            
        }

        public TextField(IWebDriver driver, IWebElement topElement) :
               base(driver, topElement)
        {
            
        }
    }
}
