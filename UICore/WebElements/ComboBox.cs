﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using ImpromptuInterface;
using ImpromptuInterface.Dynamic;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace UICore.WebElements
{
    class ComboBox : ExtJSComponent
    {
        private static By BOUND_LIST_LOCATOR = By.CssSelector("li.x-boundlist-item");

        //static Logger logger = LoggerFactory.getLogger(ComboBox.class);
        #pragma warning disable   
        static string setComboBox = "SExt.prototype.setComboBox = function (query, item, uuid) {"
			+ "var success = false;"
			+ "var comp = this.findVisibleComponent(query);"
			+ "var store = comp.getStore();"
			+ "var index = store.find(comp.displayField, item);"
			+ "if ( index != -1 ) {"
			+ "	comp.setValue(store.getAt(index));"
			+ "	success = comp.fireEvent('select', comp, [store.getAt(index)]);"
			+ "}"
			+ "writeDataToDiv(success, uuid);"
			+ "}";
        #pragma warning restore
        private static By TEXT_INPUT_LOCATOR = By.CssSelector("input.x-form-field.x-form-text");
	
	    private IWebElement input;

        private string listDynId = null;

        public ComboBox(IWebDriver driver, ExtJSQueryType queryType, String query)
            :base(driver, queryType, query)
        {
            
        }

        /**
         * @param elementContainer
         *            - locator of either parent element which wraps text input and drop down button or text input
         */
        public ComboBox(IWebDriver driver, IWebElement elementContainer) :
            base(driver, elementContainer)
        {
            

        }

        /**
         * @param optionToChoose
         *            - option to choose and additional keys to send
         */
        public void chooseOption(params string[] optionToChoose)
        {
            chooseOption(optionToChoose[0].ToString());
            for (int i = 1; i < optionToChoose.Length; i++)
            {
                getTextInput().SendKeys(optionToChoose[i]);
            }
        }

        /**
         * chooses option if one is present
         * 
         * @param optionToChoose
         *            - partial text to find among options
         */
        public void chooseOption(string optionToChoose)
        {
            clear();
            if (getListDynId() == null)
            {
                setListDynId();
            }
            IReadOnlyCollection<IWebElement> optionList = getOptionElements();
            if (optionList.Count == 0)
            {
                sendKeys(optionToChoose);
                dynamic anon = Build<ExpandoObject>.NewObject(apply: Return<bool>.Arguments<IWebDriver>(l => isDirty()));
                var IExpectedConditions = Impromptu.ActLike(anon);
                var customWait = new WebDriverWait(driver, TimeSpan.FromSeconds(2));
                customWait.PollingInterval = TimeSpan.FromMilliseconds(200) ;
                customWait.Until(IExpectedConditions);
            }
            wait.Until(Utils.ExpectedConditions.elementToBeClickable(By.CssSelector("#" + getListDynId()
                            + " li.x-boundlist-item")));
            foreach (IWebElement ele in optionList)
            {
                string actualOption;
                try
                {
                    actualOption = ele.Text.ToLower();
                }
                catch (StaleElementReferenceException)
                {
                    optionList = getOptionElements();
                    //suspicious code
                    continue;
                }
                if (actualOption.Contains(optionToChoose.ToLower()))
                {
                    ele.Click();
                    collapseDropDown();
                    wait.Until(Utils.ExpectedConditions.invisibilityOfElementLocated(By.Id(getListDynId())));
                    break;
                }
            }
        }
	
	    public void clear()
        {
            getTextInput().Clear();
        }

        private void collapseDropDown()
        {
                js.ExecuteScript(SCRIPT_TOP_ELEMENT_TO_EXT_JS_CMP
                        + " extCmp.collapse();", getTextInput());
        }

        /**
         * Finds the index of the first matching Record in this store by a specific field value
         * 
         * @param fieldName
         * @param value
         * @return
         */
        public int findInStore(string fieldName, string value)
        {
            return (int)execScriptOnExtJsCmp(string.Format("return extCmp.store.find('{0}','{1}');", fieldName, value));
        }

        public string getAttribute(string arg0)
        {
            return getTextInput().GetAttribute(arg0);
        }

        /**
         * Method getCount.
         * 
         * @return Integer
         */
        public int getCount()
        {
            string eval = (string)execScriptOnExtJsCmp("return extCmp.store.getCount();");
            if (eval == null
                    || "null".Equals(eval))
            {
                return -1;
            }

            return int.Parse(eval);

        }

        /**
         * @return web element by dynamic id of reloaded list
         */
        private IWebElement getListContainer()
        {
            if (getListDynId() == null)
            {
                setListDynId();
            }
            return driver.FindElement(By.Id(getListDynId()));
        }

        protected string getListDynId()
        {
            return listDynId;
        }

        /**
         * @return list of available option elements
         */
        private IReadOnlyCollection<IWebElement> getOptionElements()
        {
            return getListContainer().FindElements(BOUND_LIST_LOCATOR);
        }

        /**
         * @return list of available options
         */
        public List<string> getOptions()
        {
            retrieveOptions();
            List<string> optionStrList = new List<string>();
            IReadOnlyCollection<IWebElement> optionList = getOptionElements();
            foreach (IWebElement option in  optionList)
            {
                optionStrList.Add(option.Text.Trim().ToLower());
            }
            retrieveOptions();
            return optionStrList;
        }

        /**
         * return the selected value inner component
         * 
         * @return String
         */
        public string getRawValue()
        {
            return (string)execScriptOnExtJsCmp(string.Format("return extCmp.getRawValue();"));
        }

        private IWebElement getTextInput()
        {
            if (input == null)
            {
                if (!topElement.TagName.Equals("input"))
                {
                    input = topElement.FindElement(TEXT_INPUT_LOCATOR);
                }
                else
                {
                    input = topElement;
                }
            }
            return input;
        }

        /**
         * Returns the currently selected field value or empty string if no value is set.
         * 
         * @return String
         */
        public string getValue()
        {
            return (string)execScriptOnExtJsCmp(string.Format("return extCmp.getValue();"));
        }

        private bool isDirty()
        {
            return (bool)js.ExecuteScript(SCRIPT_TOP_ELEMENT_TO_EXT_JS_CMP
                    + " return extCmp.isDirty();", getTextInput());
        }

        /**
         * Resets the current field value to the originally-loaded value and clears any validation messages.
         * 
         * @return String
         */
        public bool reset()
        {
            return (bool)execScriptOnExtJsCmp(string.Format("return extCmp.reset();"));
        }

        /**
         * sends arrow key to text box
         */
        public void retrieveOptions()
        {
            sendKeys(Keys.ArrowDown);
        }

        /**
         * Method select.
         * 
         * @param i
         *            int
         */
        public void select(int i)
        {
            // focus();
            execScriptOnExtJsCmp("extCmp.setValue(extCmp.store.getAt("
                    + i
                    + ").get(extCmp.valueField) )");
            execScriptOnExtJsCmp("extCmp.fireEvent( 'select', extCmp, extCmp.store.getAt("
                    + i
                    + "), "
                    + i
                    + " )");
            // blur();
        }

        public void sendKeys(string arg0)
        {
            getTextInput().SendKeys(arg0);
        }

        /**
         * sets id of generated list with combobox options
         * Could the text input web element be a TextField component instead?
         */
        protected void setListDynId()
        {
            listDynId = (string)execScriptOnElement(SCRIPT_TOP_ELEMENT_TO_EXT_JS_CMP
                    + " extCmp.expand(); return extCmp.listKeyNav.boundList.id;", getTextInput());
        }

        /**
         * Method setValue.
         * 
         * @param value
         *            String
         * @return String
         */
        public string setValue(string value)
        {
            focus();
            execScriptOnExtJsCmp("extCmp.setValue( '"
                    + value
                    + "' )");
            execScriptOnExtJsCmp("extCmp.fireEvent( 'select', extCmp, extCmp.store.getById('"
                    + value
                    + "'), extCmp.store.indexOfId('"
                    + value
                    + "') )");
            blur();
            return value;
        }

        /**
         * @param value
         * @param fieldName
         */
        public void setValue(string value, string fieldName)
        {
            focus();
            int index = findInStore(fieldName, value);
            execScriptOnExtJsCmp("extCmp.setValue(extCmp.store.getAt("
                    + index
                    + ").get(extCmp.valueField) )");
            execScriptOnExtJsCmp("extCmp.fireEvent( 'select', extCmp, extCmp.store.getAt("
                    + index
                    + "), "
                    + index
                    + " )");
            blur();
        }
    }
}
