﻿using System;
using OpenQA.Selenium;

namespace UICore.WebElements
{
    class Button : ExtJSComponent
    {
        #pragma warning disable
        string clickButton = "SExt.prototype.clickButton = function (query, uuid) {"
            + "var comp = this.findVisibleComponent(query);"
            + "var success = comp.btnEl.dom.click();"
            + "var writeDataToDiv(success, uuid);"
            + "}";
        #pragma warning restore
        public Button(IWebDriver driver, ExtJSQueryType queryType, string query) :
            base(driver, queryType, query)
        {
            
        }

        public Button(IWebDriver driver, IWebElement topElement) :
            base(driver, topElement)
        {
            
        }

        /**
         * Method click and check if is no error after Ajax callback.
         * 
         * @throws InterruptedException
         */
        public void clickAndWaitForAjaxValid() // throws InterruptedException
        {
            waitForExecScriptToReturnTrue(".disabled == false");
            click();
            wait.Timeout = TimeSpan.FromSeconds(2);
            waitForFinishAjaxRequest();
            waitForDialogFailure();
        }

        /**
         * return true if the component is disabled
         * 
         * @return boolean
         */
        
        public override bool disabled()
        {
            return execScriptOnExtJsCmpReturnBoolean("return extCmp.disabled");
        }
    }
}
