﻿using OpenQA.Selenium;

namespace UICore.WebElements
{
    class Panel : ExtJSComponent
    {
        public Panel(IWebDriver driver, ExtJSQueryType queryType, string query) : 
            base(driver, queryType, query)
        {

        }


        public Panel(IWebDriver driver, IWebElement topElement) : 
            base(driver, topElement)
        {
            
        }

        /**
         * Method collapse.
         */
        public void collapse()
        {
            execScriptOnExtJsCmp("extCmp.collapse()");
            waitForExecScriptOnExtJSCmpToReturnTrue("return extCmp.collapsed == true");
        }

        /**
         * Method expand.
         */
        public void expand()
        {
            execScriptOnExtJsCmp("extCmp.expand()");
            waitForExecScriptOnExtJSCmpToReturnTrue("return extCmp.collapsed == false");
        }
    }
}
