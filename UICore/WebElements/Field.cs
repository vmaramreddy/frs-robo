﻿using System;
using OpenQA.Selenium;

namespace UICore.WebElements
{
    class Field : ExtJSComponent
    {
        public Field(IWebDriver driver, ExtJSQueryType queryType, string query) :
            base(driver, queryType, query)
        {
            
        }

        public Field(IWebDriver driver, IWebElement topLevel) :
               base(driver, topLevel)
        {
         
        }

        /**
         * Method getErrorText.
         * 
         * @return String
         */
        public string getErrorText()
        {
            return topElement.FindElement(By.XPath("../..//div[@class='x-form-invalid-msg']")).Text;
        }

        /**
         * Returns the raw data value which may or may not be a valid, defined value.Returns the normalized data for date field
         * 
         * @return String - theValue
         */
        public string getRawValue()
        {
            return (string)execScriptOnExtJsCmp("return el.getRawValue()");
        }

        /**
         * return the value of component
         * 
         * @return String - theValue
         */
        public string getValue()
        {
            return (string)execScriptOnExtJsCmp("return el.getValue()");
        }

        /**
         * Method hasErrorText.
         * 
         * @param err
         *            String
         * @return boolean
         */
        public bool hasErrorText(string err)
        {
            string text = getErrorText();

            return err.Equals(text);
        }

        /**
         * Resets the current field value to the originally-loaded value and clears any validation messages
         */
        public void reset()
        {
            execScriptOnExtJsCmp("el.reset()");
        }

        /**
         * Method resetValue.
         */
        public void resetValue()
        {
            execScriptOnExtJsCmp("extCmp.setValue( '' )");
        }

        /**
         * Method type.
         * 
         * @param text
         *            String
         */
        public void sendKeys(string text)
        {
            waitToLoad();

            focus();
            topElement.SendKeys(text);
            blur();
        }

        /**
         * Sets a data value into the field and validates it. To set the value directly without validation
         * 
         * @param value
         * @return String
         */
        public string setValue(string value)
        {
            return (string) execScriptOnExtJsCmp(string.Format("return el.setValue('%s')", value));
        }
    }
}
