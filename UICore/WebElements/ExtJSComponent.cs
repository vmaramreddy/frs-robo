﻿using System;
using System.Dynamic;
using UICore.Utils;
using ImpromptuInterface;
using ImpromptuInterface.Dynamic;
using OpenQA.Selenium;

namespace UICore.WebElements
{
    public class ExtJSComponent : JSExtendedWebElement
    {
        private static readonly string FUNCTION_findVisibleComponentElement = "SExt.findVisibleComponentElement = function (query) {"
            + "  var queryResultArray = (window.frames[0] &&  window.frames[0].Ext) ? window.frames[0].Ext.ComponentQuery.query(query) : Ext.ComponentQuery.query(query);"
            + "  var single = null;"
            + "  Ext.Array.every(queryResultArray, function(comp) {"
            + "	     if (comp != null && comp.isVisible(true)){"
            + "	       single = comp;"
            + "	     }"
            + "	     return (single != null);" /* return false will stop looping through array */
            + "	   });"
            + "  var el = (single != null ? single.getEl().dom : null);"
            + "  return el;"
            + "}";

        /**
	     * This is meant to be a general way to convert a specific id 
         to the general id ( Ex. td[@id="combobox-1009-bodyEl"] to comboBox-1009)
	    */
        protected internal static readonly string SCRIPT_TOP_ELEMENT_TO_EXT_JS_CMP = "var id = el.id"
            + ".replace(/-\\w*El$/,'')" /* -inputEl, -bodyEl, -anythingEl */
            + ".replace('-triggerWrap','');"
            + "var extCmp = Ext.getCmp(id);";


        private static readonly string SCRIPT_TOP_ELEMENT_TO_EXT_JS_ID = SCRIPT_TOP_ELEMENT_TO_EXT_JS_CMP
            + ";return extCmp.getId();"; // should this be .id?

        /**
	     * ExtJS Query that would be used in Ext.ComponentQuery.query("[name='myCheckbox']");
	    */
        protected internal static string convertQueryTypeAndQueryToScript(ExtJSQueryType queryType, string query)
        {
            string queryScript = null;

            switch (queryType)
            {
                case ExtJSQueryType.ComponentQuery:
                    queryScript = string.Format("%s; %s; return SExt.findVisibleComponentElement(\"%s\");",
                    FUNCTION_DEFINE_SExt, FUNCTION_findVisibleComponentElement, query);
                    break;

                case ExtJSQueryType.Custom:
                    queryScript = string.Format("%s; %s; return SExt.findVisibleComponentElement(\"%s\");",
                    FUNCTION_DEFINE_SExt, FUNCTION_findVisibleComponentElement, query);
                    break;

                case ExtJSQueryType.GetCmp:
                    queryScript = string.Format("return Ext.getCmp(\"%s\").getEl().dom;", query);
                    break;

                default: queryScript = query; break;

            }
            return queryScript;
        }

        protected internal string extJsCmpId = null;

        protected internal string FUNCTION_getXPathTo = "SExt.getXPathTo = function getXPathTo(element) {"
            + "    if (element.id!=='')"
            + "        return 'id(\"'+element.id+'\")';"
            + "    if (element===document.body)"
            + "        return element.tagName;"
            + "    var ix= 0;"
            + "    var siblings= element.parentNode.childNodes;"
            + "    for (var i= 0; i<siblings.length; i++) {"
            + "        var sibling= siblings[i];"
            + "        if (sibling===element)"
            + "            return SExt.getXPathTo(element.parentNode)+'/'+element.tagName+'['+(ix+1)+']';"
            + "        if (sibling.nodeType===1 && sibling.tagName===element.tagName)"
            + "            ix++;"
            + "    }"
            + "}";

        /**
	      * @param driver
	      * @param getCmpQuery
	      * ExtJS Query that would be used in Ext.getCmp('combobox-1001');
	      */
        public ExtJSComponent(IWebDriver driver, ExtJSQueryType queryType, string query) :
            base(driver, convertQueryTypeAndQueryToScript(queryType, query))
        {
            if (queryType.Equals(ExtJSQueryType.GetCmp))
            {
                this.extJsCmpId = query;
            }
        }

        /**
	      * @param driver
	      * @param topElement
	      * locator of either parent topElement which wraps text input and drop down button or text input
	      */
        public ExtJSComponent(IWebDriver driver, IWebElement topElement) :
               base(driver, topElement)
        {

        }

        public void blur()
        {
            fireEvent("blur");
        }

        /**
	    * This is used to run javascript scrits on the ExtJS ExtJSComponent. 
        * For Example, execScriptOnExtJsComponent("return extCmp.getValue()"); will run the
	    * JavaScript method getValue on the ExtJS component object.
	    * 
	    * @param jsCode
	    * Javscript code to execute.
	    * @return
	    */
        public object execScriptOnExtJsCmp(string jsCode)
        {
            string finalScript = string.Format("var extCmp = Ext.getCmp('%s'); %s;", getComponentId(), jsCode);
            return execScriptClean(finalScript);
        }

        public bool execScriptOnExtJsCmpReturnBoolean(string jsCode)
        {
            string finalScript = string.Format("var extCmp = Ext.getCmp('%s'); %s;", getComponentId(), jsCode);
            return execScriptCleanReturnBoolean(finalScript);
        }

        /**
	     * Method evalNullComponent.
	     * 
	     * @param expr
	     * String
	     * @return boolean
	     */
        public bool execScriptOnExtJSCmpReturnIsNull(string jsCode)
        {
            string finalScript = string.Format("var extCmp = Ext.getCmp('%s'); %s;", getComponentId(), jsCode);
            try
            {
                object output = execScriptOnExtJsCmp(finalScript);
                return output == null
                        || "null".Equals(output);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string execScriptOnExtJsCmpReturnString(string jsCode)
        {
            string finalScript = String.Format("var extCmp = Ext.getCmp('%s'); %s;", getComponentId(), jsCode);
            return execScriptCleanReturnBoolean(finalScript).ToString();
        }


        public void fireEvent(string eventFire)
        {

            Selenium.Internal.SeleniumEmulation.JavaScriptLibrary.CallEmbeddedSelenium
                (driver, "triggerEvent", topElement, eventFire);
        }

        /**
	     * Method focus.
	     * 
	     * @deprecated remove all event methods
	     */

        public void focus()
        {
            fireEvent("focus");
        }

        /**
	      * return the componentId
	      * 
	      * @return String
	    */
        public string getComponentId()
        {
            if (extJsCmpId == null)
            {
                // well then we better have the WebElement!
                if (topElement == null)
                    throw new Exception("Neither extJsCmpId or topElement has been set");
                extJsCmpId = (string)execScriptOnTopLevelElement(SCRIPT_TOP_ELEMENT_TO_EXT_JS_ID);
            }
            return extJsCmpId;
        }

        /**
	      * Returns the Ext.Element which encapsulates this ExtJSComponent.
	      * 
	      * @return String
	    */
        public IWebElement getElDom()
        {
            return (IWebElement)execScriptOnExtJsCmp("return extCmp.getEl().dom");
        }

        /**
	     * Returns the absolute expression that resolves this proxy's Ext component.
	     * 
	     * @return String
	    */
        public string getExpression()
        {
            return string.Format("window.Ext.getCmp('%s')", getComponentId());
        }

        /**
	     * Returns an XPath to the Ext component, which contains the ID provided by getId()
	     * 
	     * @return String
	    */
        public string getXPath()
        {
            return string.Format("%s return getPathTo(%s", FUNCTION_getXPathTo, topElement);
        }

        /**
	     * Gets the xtype for this component as registered with Ext.ComponentMgr ------------
	     * 
	     * @return String
	    */
        public string getXType()
        {
            return (string)execScriptOnExtJsCmp("return el.getXType()");
        }

        /**
	     * Returns this ExtJSComponent's xtype --hierarchy-- as a slash-delimited string ------------
	     * 
	     * @return List<String>
	    */
        public string getXTypes()
        {
            return (string)execScriptOnExtJsCmp("return el.getXTypes()");
        }

        /**
         * Method hidden.
         * 
         * @return boolean
         */
        public bool hidden()
        {
            return execScriptOnExtJsCmpReturnBoolean("return extCmp == null")
                    || execScriptOnExtJsCmpReturnBoolean("return extCmp.hidden");
        }

        /**
	     * return true if the component is Disabled
	     * 
	     * @return boolean
	     */
        public virtual bool isDisabled()
        {
            return (bool)execScriptOnExtJsCmp("return extCmp.disabled");
        }

        /**
	     * Method visible.
	     * 
	     * @return boolean
	     */
        public bool visible()
        {
            return !hidden();
        }

        /**
	     * Method waitFor msg dialog when has an error/failure.
         */
        public void waitForDialogFailure()
        {
            js.ExecuteScript("window.Ext.Msg.getDialog()");

            try
            {
                waitForExecScriptToReturnTrue("window.Ext.Msg.isVisible() == false");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /**
	     * Runs a script on an ext JS componenet. If it returns false, it waits, then tries again.
	     * Up to timeoutSecond.
	     * 
	     * @param expr
	     *            String
	     */
        protected void waitForExecScriptOnExtJSCmpToReturnTrue(string expr)
        {
            string fullExpr = getExpression()
                    + expr;
            waitForExecScriptToReturnTrue(fullExpr);
        }

        /**
	     * Method what until all ajax requests are finshed .
	     * 
	     * @return boolean return true if there is no ajax request on AIRR
	     */

        protected internal override bool waitForFinishAjaxRequest()
        {
            // DO NOT use helper methods. go straight to js
            long start = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            long end = start + timeOutInSeconds * 1000;
            bool ret = false;
            while ((DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond) <= end)
            {
                try
                {
                    if (bool.FalseString.Equals(js.ExecuteScript("return Ext.Ajax.isLoading()")))
                    {
                        ret = true;
                        break; // should not run the sleep below.
                    }
                }
                catch (Exception)
                {
                    // ignore
                }
                try
                {
                    System.Threading.Thread.Sleep((int)sleepInMillis);
                }
                catch (Exception)
                {
                    // ignore
                }
            }
            return ret;
        }


        /**
	     * Wait until the Grid Loading Mask is not visible any more
	     * 
	     * @param componentId
	     *            String
	     * @return boolean
	     */
        protected bool waitForGridLoadingMaskToDisappear(string componentId)
        {
            bool success = waitForExecScriptToReturnTrue
                (string.Format("Ext.getCmp('%s').getEl().isMasked()", componentId));

            return success;
        }

        /**
	     * Method waitForHidden.
	     */
        public void waitForHidden()
        {
            dynamic anon = Build<ExpandoObject>.NewObject(checkcondition: Return<bool>.Arguments<long>(l => hidden()));
            IWaitCondition waitCondition = Impromptu.ActLike(anon);
            bool success = ThreadUtils.waitFor(waitCondition, 15);
            if (!success)
            {
                throw new Exception("RuntimeException : Timeout, could wait no longer for hidden component.");
            }
        }

        /**
	     * Wait until the component is viable
	     * 
	     * @return boolean
	     */
        public bool waitForIsVisible()
        {
            dynamic anon = Build<ExpandoObject>.NewObject(checkcondition: Return<bool>.Arguments<long>(l => visible()));
            IWaitCondition waitCondition = Impromptu.ActLike(anon);
            bool success = ThreadUtils.waitFor(waitCondition, 15);
            if (!success) {
			    throw new Exception("Timeout");
            }
		
		    return success;
        }


        /**
         * Wait For Tree Loading Mask To Disappear.
         * 
         * @param panelMaskId
         *            String
         * @return boolean
         */
        protected internal bool waitForTreeLoadingMaskToDisappear(string panelMaskId)
        {
            bool success = waitForExecScriptToReturnTrue(string.Format("!window.Ext.fly('%s').isVisible()", panelMaskId));
            return success;
        }

        /**
         * Wait for component. until the component is enable again.
         */
        public void waitToLoad()
        {
            waitForExecScriptOnExtJSCmpToReturnTrue("extCmp.disabled != true");
        }
    }
}
