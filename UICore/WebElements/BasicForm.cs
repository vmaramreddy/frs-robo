﻿using System;
using OpenQA.Selenium;

namespace UICore.WebElements
{
    class BasicForm : ExtJSComponent
    {
        public BasicForm(IWebDriver driver, ExtJSQueryType queryType, String query) : 
            base(driver, queryType, query)
        {
            
        }

        public BasicForm(IWebDriver driver, IWebElement topElement) : base(driver, topElement)
        {
            
        }

        public ExtJSComponent findComponentIn(ExtJSQueryType queryType, String query)
        {
            return new ExtJSComponent(driver, queryType, query);
        }
    }
}
