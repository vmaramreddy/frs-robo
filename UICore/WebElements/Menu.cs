﻿using OpenQA.Selenium;

namespace UICore.WebElements
{
    class Menu : ExtJSComponent
    {
        public Menu(IWebDriver driver, ExtJSQueryType queryType, string query)
            :  base (driver, queryType, query)
        {
            
        }

        public Menu(IWebDriver driver, IWebElement topElement) :
                    base (driver, topElement)
        {
            
        }

        /**
         * Method click.
         * 
         * @param itemKey
         *            String
         */
        public void click(string itemKey)
        {
            string id = (string) execScriptOnExtJsCmp("el.items.items["
                    + getExpression()
                    + ".items.indexOfKey('"
                    + itemKey
                    + "')].el.dom.id");
            driver.FindElement(By.XPath("//*[@id='" + id + "']")).Click();
        }

        /**
         * Method click.
         * 
         * @param propName
         *            String
         * @param propValue
         *            String
         */
        public void click(string propName, string propValue)
        {
            string id = (string)(execScriptOnExtJsCmp("return extCmp.items.items["
                        + getExpression()
                        + ".items.findIndex('"
                        + propName
                        + "', '"
                        + propValue
                        + "')].el.dom.id"));

            topElement.FindElement(By.XPath("//*[@id='" + id + "']")).Click();
        }
    }
}
