﻿using OpenQA.Selenium;

namespace UICore.WebElements
{
    class TriggerField : ExtJSComponent
    {
        /**
	     * Field trigger.
	     */
        private Button trigger;

        public TriggerField(IWebDriver driver, ExtJSQueryType queryType, string query) :
               base (driver, queryType, query)
        {
            trigger = new Button(driver, queryType, query);
        }

        public TriggerField(IWebDriver driver, IWebElement topElement) :
               base (driver, topElement)
        {
            trigger = new Button(driver, ExtJSQueryType.GetCmp, getExpression()
                    + ".trigger");
        }

        /**
         * Method clickTrigger.
         * 
         * @return TriggerField
         */
        public TriggerField clickTrigger()
        {
            trigger.click();
            return this;
        }
    }
}
