﻿using OpenQA.Selenium;

namespace UICore.WebElements
{
    class Tree : ExtJSComponent
    {
        public Tree(IWebDriver driver, ExtJSQueryType queryType, string query) :
               base(driver, queryType, query)
        {
            
        }

        public Tree(IWebDriver driver, IWebElement topElement) :
               base (driver, topElement)
        {
            
        }

        /**
         * Method contains.
         * 
         * @param id
         *            String
         * @return boolean
         */
        public bool contains(string id)
        {
            return execScriptOnExtJsCmpReturnBoolean("return extCmp.nodeHash['"
                    + id
                    + "'] != null");
        }

        /**
         * Method getErrorText.
         * 
         * @return String
         */
        public string getErrorText()
        {
            IWebElement errorText = driver.FindElement(By.XPath(getXPath()
                    + "//div[@class='x-form-invalid-msg']"));
            string text = errorText.Text;
            return text;
        }

        /**
         * Method getRootNode.
         * 
         * @return TreeNode
         */
        public TreeNode getRootNode()
        {
            TreeNode treeNode = new TreeNode(driver, ExtJSQueryType.Custom, getExpression());
            return treeNode.getRootNode();
        }

        /**
         * Method hasErrorText.
         * 
         * @param err
         *            String
         * @return boolean
         */
        public bool hasErrorText(string err)
        {
            string text = getErrorText();
            return err.Equals(text);
        }

        /**
         * Method select By Id.
         * 
         * @param id
         *            String
         * @return TreeNode
         */
        public TreeNode select(string id)
        {
            execScriptClean(".getSelectionModel().select(" //
                            + getExpression()
                            + ".nodeHash['"
                            + id
                            + "']"
                            + //
                            ")");
            TreeNode treeNode = new TreeNode(driver, ExtJSQueryType.Custom, getExpression());
            return treeNode.getSelectedNode();
        }
    }
}
