﻿using OpenQA.Selenium;

namespace UICore.WebElements
{
    class TreeGrid : ExtJSComponent
    {
        public TreeGrid(IWebDriver driver, ExtJSQueryType queryType, string query) :
               base(driver, queryType, query)
        {
           
        }

        public TreeGrid(IWebDriver driver, IWebElement topElement) :
               base (driver, topElement)
        {
            
        }

        /**
         * (search in node attribute) return True if found any match
         * 
         * @param id
         *            - nodeId
         * @return boolean
         */
        public bool contains(string id)
        {
            return execScriptCleanReturnBoolean("return extCmp.nodeHash['"
                    + id
                    + "'] != null");
        }

        /**
         * Method getErrorText.
         * 
         * @return String
         */
        public string getErrorText()
        {
            IWebElement el = driver.FindElement(By.XPath(getXPath()
                           + "//div[@class='x-form-invalid-msg']"));
            string text = el.Text;
            return text;
        }

        /**
         * return the root-node of the tree
         * 
         * @return - node
         */
        public TreeNode getRootNode()
        {
            TreeNode treeNode = new TreeNode(driver, ExtJSQueryType.Custom, getExpression());
            return treeNode.getRootNode();
        }

        /**
         * return the selected Treenode
         * 
         * @return TreeNode
         */
        public TreeNode getSelectedNode()
        {
            TreeNode node = new TreeNode(driver, ExtJSQueryType.Custom, getExpression());
            return node.getSelectedNode();
        }

        /**
         * Method hasErrorText.
         * 
         * @param err
         *            String
         * @return boolean
         */
        public bool hasErrorText(string err)
        {
            string text = getErrorText();
            return err.Equals(text);
        }

        /**
         * Select the ui-node by given nodeId
         * 
         * @param id
         *            - nodeId
         * @return TreeNode
         */
        public TreeNode select(string id)
        {
            execScriptClean(".getSelectionModel().select(" //
                            + getExpression()
                            + ".nodeHash['"
                            + id
                            + "']"
                            + //
                            ")");
            TreeNode treeNode = new TreeNode(driver, ExtJSQueryType.Custom, getExpression());
            return treeNode.getSelectedNode();
        }

        /**
         * select node by given string
         * 
         * @param name
         * @return TreeNode
         */
        public TreeNode selectNodeByName(string name)
        {
            TreeNode treeNode = new TreeNode(driver, ExtJSQueryType.Custom, getExpression());
            treeNode.getRootNode().findNodeGridChild(name);
            return treeNode.getSelectedNode();
        }

        /**
         * Method whaitForloading.
         */
        public void waitForloading()
        {
            waitForTreeLoadingMaskToDisappear("loading-mask");
        }
    }
}
