﻿using System;
using System.Threading;
using OpenQA.Selenium;
using Selenium;

namespace UICore.Utils
{
    class ExtJsUtils
    {
        /**
	     * return reference to extjs component
	     * 
	     * @param componentId
	     * @return
	     */
        public static string getComponentById(string componentId)
        {
            return "window.Ext.getCmp('"
                    + componentId
                    + "')";
        }

        public static string getComponentByTextOrLable(ISelenium selenium, string textOrLable, Xtype xtype)
        {
            string component = "";
            if (textOrLable != null)
            {
                component = selenium.GetEval(string.Format("window.findComponentByText('%s','%s')", textOrLable, xtype.ToString().ToLower()));
            }
            else
            {
                component = selenium.GetEval(string.Format("window.findComponentByText(null,'%s')", xtype.ToString().ToLower()));
            }
            return getComponentById(component);
        }

        /**
	     * just return array of id's be aware - (After you got the result you need to run getComponentExpression with component id to get the reference to the
	     * component
	     * 
	     * @param selenium
	     * @param xtype
	     * @return
	     */

        public static string[] getComponentIdsByType(ISelenium selenium, Xtype xtype)
        {
            string[] component = null;
            component = selenium.GetEval(string.Format("window.findComponentByText(null,'%s')", xtype.ToString().ToLower())).Split(',');
            return component;
        }

        public static string getComponentType(ISelenium selenium, string textOrLable)
        {
            return selenium.GetEval(string.Format("%s.getXType()", getComponentByTextOrLable(selenium, textOrLable)));
        }

        private static object getComponentByTextOrLable(ISelenium selenium, string textOrLable)
        {
            string component = "";
            component = selenium.GetEval(string.Format("window.findComponentByText('%s',null)", textOrLable));
            return getComponentById(component);
        }

        public static string getComponentXpath(IWebDriver driver, string element)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            return (string)js.ExecuteScript(string.Format("return window.createXPathFromElement(%s)", element));
        }

        public static bool waitForAjaxRequests(ISelenium selenium)
        {
            for (int second = 0; second <= 20; second++)
            {

                try
                {
                    string result = selenium.GetEval("window.verifyNoAjaxCalls()");
                    if ("false".Equals(result))
                    {
                        Thread.Sleep(1000);

                    }
                    else
                    {
                        return true;
                    }

                }
                catch (Exception) {

                }
            }
		    return false;
	    }
    }
}
