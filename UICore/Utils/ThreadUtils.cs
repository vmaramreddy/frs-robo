﻿using System;
namespace UICore.Utils
{
    public class ThreadUtils
    {

        public static void sleep(long duration)
        {
            try
            {
                System.Threading.Thread.Sleep(TimeSpan.FromSeconds(duration));
            }
            catch (System.Threading.ThreadInterruptedException)
            {
                System.Threading.Thread.CurrentThread.Interrupt();
            }
        }

        public static bool waitFor(IWaitCondition condition)
        {
            bool result = false;
            if (condition != null)
            {
                long startTime = GetCurrentTimeMillis();

                while (!(result = condition.checkCondition(GetCurrentTimeMillis()
                        - startTime)))
                {
                    try
                    {
                        System.Threading.Thread.Sleep(100);
                    }
                    catch (System.Threading.ThreadInterruptedException)
                    {
                        System.Threading.Thread.CurrentThread.Interrupt();
                    }
                }
            }

            return result;
        }

        public static bool waitFor(IWaitCondition condition, long timeoutDuration)
        {
            timeoutDuration = ToMillis(timeoutDuration);
            bool result = false;
            if (condition != null)
            {
                long startTime = GetCurrentTimeMillis();
                long curTime = startTime;
                while (!(result = condition.checkCondition(curTime
                        - startTime))
                        && (curTime
                                - startTime < timeoutDuration))
                {
                    try
                    {
                        System.Threading.Thread.Sleep(100);
                    }
                    catch (System.Threading.ThreadInterruptedException)
                    {
                        System.Threading.Thread.CurrentThread.Interrupt();
                    }
                    curTime = GetCurrentTimeMillis();
                }
            }
            return result;
        }

        public static bool waitFor(IWaitCondition condition, long timeoutDuration, long sleepDuration)
        {
            long timeout = ToMillis(timeoutDuration);
            long sleepBetween = ToMillis(sleepDuration);
            bool result = false;
            if (condition != null)
            {
                long startTime = GetCurrentTimeMillis();
                long curTime = startTime;

                while (!(result = condition.checkCondition(curTime
                        - startTime))
                        && (curTime
                                - startTime < timeout))
                {
                    try
                    {
                        System.Threading.Thread.Sleep((int)sleepBetween);
                    }
                    catch (System.Threading.ThreadInterruptedException)
                    {
                        System.Threading.Thread.CurrentThread.Interrupt();
                    }
                    curTime = GetCurrentTimeMillis();
                }
            }

            return result;
        }



        public static long GetCurrentTimeMillis()
        {
            DateTime Jan1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan javaSpan = DateTime.UtcNow - Jan1970;
            return (long)javaSpan.TotalMilliseconds;
        }

        public static long ToMillis(long timeOutDuration)
        {
            DateTime Jan1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan javaSpan = DateTime.UtcNow.AddMilliseconds(timeOutDuration * 1000) - Jan1970;
            return (long)javaSpan.TotalMilliseconds;
        }
    }

}
