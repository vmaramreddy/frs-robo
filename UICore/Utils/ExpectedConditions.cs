﻿using System;
using OpenQA.Selenium;

namespace UICore.Utils
{
    class ExpectedConditions 
    {
        public static Func<IWebDriver, IWebElement> elementToBeClickable(By locator)
        {
            return (driver) =>
            {
                try
                {
                    if (driver.FindElement(locator).Displayed && driver.FindElement(locator).Enabled)
                    {
                        return driver.FindElement(locator);
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (StaleElementReferenceException)
                {
                    return null;
                }
            };
        }

        public static Func<IWebDriver, bool> invisibilityOfElementLocated(By locator)
        {
            return (driver) =>
            {
                try
                {
                    return !(driver.FindElement(locator).Displayed);
                }
                catch (NoSuchElementException)
                {
                    // Returns true because the element is not present in DOM. The
                    // try block checks if the element is present but is invisible.
                    return true;
                }
                catch (StaleElementReferenceException)
                {
                    // Returns true because stale element reference implies that element
                    // is no longer visible.
                    return true;
                }
            };
        }

    }
}
