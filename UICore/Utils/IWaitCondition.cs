﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UICore.Utils
{
    public interface IWaitCondition
    {
        bool checkCondition(long elapsedTimeInMs);
    }
}
