﻿using System;
using OpenQA.Selenium;

namespace UICore.Utils
{
    interface IExpectedConditions
    {
        bool apply();
        
    }
}
